import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {AppGlobals} from '../../app-globals.service';
import { LoaderService } from '../loader/loader.service';
import { User } from 'src/app/models/user';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PeticionesService {
  private usersSubject = new BehaviorSubject([]);
  private users: User[];

  constructor(private http: HttpClient, private globals: AppGlobals, private loader: LoaderService) { }

mostrarLoader() {
  this.loader.show();
}

getUsers(): Observable<User[]> {

  /*this.mostrarLoader();

  const url = `${this.globals.apiPath}${this.globals.getUsers}`;
  let  headers = new HttpHeaders();
  headers = headers.set('Content-Type', 'application/json');
  headers = headers.set('Authorization', String(localStorage.getItem('token')));
  //return  this.http.get(url, {headers}); */

  this.mostrarLoader();
  return this.usersSubject.asObservable();
}

private refresh() {
  // Emitir los nuevos valores para que todos los que dependan se actualicen.
  this.usersSubject.next(this.users);
}

createNewUser(user: User) {
  this.users = [...this.users, user];
  this.refresh();
}

loadDummyData() {
  this.users = this.globals.users;
  this.refresh();
}

approveAll() {
 
  
  this.users = this.users.map(user => Object.assign({}, user, { isPremium: true }));
  this.refresh();
}

}
