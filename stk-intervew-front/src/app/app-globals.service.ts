import { User } from './models/user';

export class AppGlobals {
    ISPROD = true;


    // Enviroments
    updateUser: String = 'update/user';
    addUser: String = 'create/user';
    getUsers: String = 'get/user/';
    deleteUser: String = 'delete/user/';

  
    apiPath: String = this.getUrltWs();
   
     getUrltWs(): String  {
        let retornar = null
        if (this.ISPROD) {
            retornar =   'http://algunaurldesofttek/api/';  // server
           } else {
            retornar = 'http://algunaurldemipc/api/'; // local
          }



        return retornar;
     }

     //Global Bats

   users:Array<User> = [
      {firstName:"Diego",lastName:"Perez",address:"calle#1 col. Colonia CP 2000",mail:"diegomacias@live.com.mx",phone:"4493738713"},
      {firstName:"Pedro",lastName:"Macias",address:"calle#3 col. Colonia CP 2000",mail:"pedro.m@live.com.mx",phone:"4493738713"},
      {firstName:"Juan",lastName:"Rodriguez",address:"calle#14 col. Colonia CP 2000",mail:"rod.juan@live.com.mx",phone:"4493738713"},
      {firstName:"Karen",lastName:"Smith",address:"calle#15 col. Colonia CP 2000",mail:"armando@live.com.mx",phone:"4493738713"},
      {firstName:"Armando",lastName:"de Lira",address:"calle#16 col. Colonia CP 2000",mail:"de.lira.juan@live.com.mx",phone:"4493738713"},
      {firstName:"Michel",lastName:"Nieves",address:"calle#17 col7. Colonia CP 2000",mail:"mnieves@live.com.mx",phone:"4493738713"},
      {firstName:"Jose",lastName:"Fernandez",address:"calle#18 col. Colonia CP 2000",mail:"ferjos@live.com.mx",phone:"4493738713"},
      {firstName:"Juan Armando",lastName:"Milk",address:"calle#19 col9. Colonia CP 2000",mail:"juanarmasmilk@live.com.mx",phone:"4493738713"},
      {firstName:"Jose Michel",lastName:"Roshi",address:"calle#19 col9. Colonia CP 2000",mail:"michjosroshi@live.com.mx",phone:"4493738713"},
      {firstName:"Karen Michel",lastName:"Macias",address:"calle#110 col11. Colonia CP 2000",mail:"k.michel@live.com.mx",phone:"4493738713"}
    ];




}
