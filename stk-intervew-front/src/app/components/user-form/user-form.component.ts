import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { User } from 'src/app/models/user';
import { PeticionesService } from 'src/app/services/http/peticiones.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  regiForm: FormGroup;
  firstName: string = '';
  lastName: string = '';
  address: string = '';
  mail: string = '';

  constructor(private fb: FormBuilder, private peticionesService: PeticionesService) {
    this.regiForm = fb.group({
      'firstName': [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]+$')])],
      'lastName': [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$') ])],
      'address': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(500)])],
      'mail': [null, Validators.compose([Validators.required, Validators.email])],
      'phone': [null, Validators.compose([Validators.required, Validators.minLength(10),Validators.maxLength(10),Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$') ])]
    });
  }

  onFormSubmit(form: NgForm) {

    let user: User = this.regiForm.getRawValue();
    let phone = user.phone;
    let primerGrupo = phone.substring(0,3);
    let segundoGrupo = phone.substring(3,4)
    let tercerGrupo = phone.substring(4,6)
    let cuartoGrupo = phone.substring(6,10)

    let newPhone = '('+primerGrupo+')'+'-'+segundoGrupo+tercerGrupo+'-'+cuartoGrupo;

    user.phone = newPhone;
    this.peticionesService.createNewUser(user);

  }

  ngOnInit() {
  }


  transform(val, args) {
    val = val.charAt(0) != 0 ? '0' + val : '' + val;
    let newStr = '';

    for(var i=0; i < (Math.floor(val.length/2) - 1); i++){
       newStr = newStr+ val.substr(i*2, 2) + '-';
    }
    return newStr+ val.substr(i*2);
}




}
