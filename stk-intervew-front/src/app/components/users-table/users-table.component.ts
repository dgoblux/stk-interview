import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { User } from 'src/app/models/user';
import { PeticionesService } from 'src/app/services/http/peticiones.service';
import { LoaderService } from 'src/app/services/loader/loader.service';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {
  title = 'interview';
  displayedColumns: string[] = ['firstName', 'lastName', 'address', 'mail','phone'];
  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


users:Array<User>;

  constructor( private peticionesService: PeticionesService,private loader: LoaderService) {

    this.peticionesService.getUsers().subscribe(users => {
      console.log(users);
      this.dataSource = new MatTableDataSource(users);
      this.loader.hide();
      this.sortAndPaginator(); 
     });
     this.peticionesService.loadDummyData();
  }

sortAndPaginator() {

  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
